# Tutorials and use cases for data handling

## Information for model data users

Welcome to the tutorials and use cases repository! 

In the folder "notebooks" you can find Jupyter notebooks with coding examples showing how to use Big Data and High-Performance Computing software for applications in geoscience. 

The Jupyter notebooks are meant to run in the Jupyterhub server of the German Climate Computing Center [DKRZ](https://www.dkrz.de/) which is an [ESGF](https://esgf.llnl.gov/) repository that hosts 4 petabytes of [CMIP6](https://pcmdi.llnl.gov/CMIP6/) model data (more info on the data pool [here](https://www.dkrz.de/up/services/data-management/cmip-data-pool)). 

See in this [video](https://youtu.be/f0wZX9i0uWQ) the main features of the DKRZ Jupterhub/lab and how to use it. Clone this tutorial-and-use-cases repo in your home at the DKRZ supercomputer "Mistral" or just download from here and upload there the notebook of your interest. Then, the repo or just the notebook will be visible from the Jupyterhub. When you open a notebook in the Jupyterhub, please, choose the Python 3 unstable kernel on the Kernel tab (upper tool bar in the Jupyterhub). This kernel contains all the common geoscience packages. 

Besides the information on the Jupyterhub, in these DKRZ [docs](https://www.dkrz.de/up/systems/mistral/programming/jupyter-notebook) you can find how to run Jupyter notebooks directly in the DKRZ server, that is, out of the Jupyterhub. It will entail that you create the environment accounting for the required package dependencies. Do not try to run these notebooks in your premise, which is also known as [client-side](https://en.wikipedia.org/wiki/Client-side) computing. It will also require that you install the necessary packages on you own but it will anyway fail because you will not have direct access to the data pool. Direct access to the data pool is one of the main benefits of the [server-side](https://en.wikipedia.org/wiki/Server-side) data-near computing demonstrated in these tutorials and use cases :relaxed:. 


## Information for notebook developers

This project aims at providing short (5min) jupyternotebooks about all parts of data handling at DKRZ. The focus is to show the feasibility of simple analysis. 

Since this project is **public**, the notebooks must be:
- clear and
- maintained

We provide an environment.yml to make the repo portable.

#### Developers workflow (following the [gitlab guidelines](https://about.gitlab.com/blog/2017/03/17/demo-mastering-code-review-with-gitlab/)):

1. Create an issue in the Kanban board.
2. Modify the repo in the development branch. That development branch can be of the these two options: 
    - a temporal development branch in this tutorials-and-use-cases repo that you created for that particular contribution and you enable the "delect this branch after merge" option in the merge request, or 
    - a development branch in your forked repo, and you decide if you want to delect it or not, it is your fork, your rules, just be sure that your fork is synchronized with this repo before you modify it, we have the gitlab Community Edition, check the Git CLI info on how to synchronize your fork [here](https://forum.gitlab.com/t/refreshing-a-fork/32469/2)).
3. Request a merge from your developer branch to the tutorials-and-use-cases master and assign at least one reviewer.
4. Mark the issue as done in the Kanban.
5. The stand-up meeting decides if it is merged to master and the issue is closed or some interations of steps 2 and 3 are needed.

It is hard for git to track the version of images and also they require to much space. In general, we do not need here the output of the cells. Therefore, please, be sure that you "Restart & Clear Output" before you commit your changes.

#### Notebooks name convention:

TYPE_PURPOSE_TOOL_(OBJECT) where:
- type is either "tutorial" or "use-case", 
- purpose is e.g. "find-data" or "analyse", 
- tool is "intake" or "python" or "cdo", 
- object is "netCDF", "catalogs", "cmip6",...